#include <stdio.h>
#include <stdlib.h>

void MakeDataSet(int n) {
    FILE *fptr;
    fptr = fopen("DataSet2_a.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    int i, j;

    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++) {
            if (j == n - 1)
                if (i == n - 1) fprintf(fptr, "%d", rand() % 10);
                else fprintf(fptr, "%d\n", rand() % 10);
            else
                fprintf(fptr, "%d ", rand() % 10);
        }

    fclose(fptr);
}

void MakeEmptyDataSet(int n) {
    FILE *fptr;
    fptr = fopen("DataSet2_C.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    int i, j;

    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++) {
            if (j == n - 1)
                if (i == n - 1) fprintf(fptr, "%d", rand() % 10);
                else fprintf(fptr, "%d\n", rand() % 10);
            else
                fprintf(fptr, "%d ", rand() % 10);
        }

    fclose(fptr);
}

void MakeDataSet_new(int n){
    FILE *fptr;
    fptr = fopen("DataSet22c.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    int i, j;

    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++) {
            if (j == n - 1)
                //if (i == n - 1) fprintf(fptr, "%d\n", rand() % 10);
                //else
                    fprintf(fptr, "%d\n", rand() % 10);
            else
                fprintf(fptr, "%d ", rand() % 10);
        }

    fclose(fptr);
}


int main (){

    MakeDataSet_new(100);

    return 0;
}
